package update.pullrequest;

import akka.actor.ActorRef;
import models.Contribution;
import models.ContributionEvent;
import models.ContributionEventType;
import models.State;
import models.TagTest;
import models.UserTest;
import org.joda.time.DateTime;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.Collection;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * PullRequestImporterEventTest
 */
public class PullRequestImporterEventTest extends AbstractPullRequestImporterTest {

    private static final String TEST_REPOSITORY_ID = "123";
    private static final String TEST_PULL_REQUEST_ID = "123";
    private static final String TEST_CONTRIBUTION_NAME = "Major Mods!";
    private static final String TEST_CONTRIBUTION_DESCRIPTION = "Some change to the code";
    private static final DateTime TEST_CONTRIBUTION_CREATED_ON = DateTime.now();
    private static final DateTime TEST_CONTRIBUTION_UPDATED_ON = DateTime.now();
    private static final String TEST_BRANCH_NAME = "sigh";

    @Test
    public void importAllPullRequests_publishesContributionGeneratedEvents() throws Exception {
        // setup PullRequestImporter actor
        ActorRef pullRequestImporterActor = actorRule.actorOf(PullRequestImporter.class);
        eventStream.subscribe(pullRequestImporterActor, ImportPullRequestsEvent.class);

        // setup receiver
        Future<ContributionEvent> response = actorRule.expectMsg(ContributionEvent.class, SECONDS.toMillis(10));

        // trigger pull request import from json
        eventStream.publish(new ImportPullRequestsEvent(firstResponse, repositoryId));

        // verify events sent
        ContributionEvent reply = Await.result(response, Duration.Inf());
        assertEquals(ContributionEventType.CONTRIBUTION_GENERATED, reply.contributionEventType);
        assertEquals(State.OPEN, reply.contribution.state);
    }

    @Test
    public void shouldReturn_One_ContributionEventFilteredByType_CONTRIBUTION_GENERATED() {
        Contribution contribution = createTestInstance();
        contribution.save();

        Collection<ContributionEvent> contributionEvents = contribution.contributionEvents;
        assertThat(contributionEvents.size(), is(1));

        ContributionEvent firstContributionEvent = contributionEvents.iterator().next();
        assertThat(firstContributionEvent.contributionEventType,
                   is(ContributionEventType.CONTRIBUTION_GENERATED));
    }

    public static Contribution createTestInstance() {
        Contribution contribution = new Contribution(TEST_REPOSITORY_ID, TEST_PULL_REQUEST_ID,
                                                     TEST_CONTRIBUTION_NAME, TEST_CONTRIBUTION_DESCRIPTION, UserTest.createTestInstance(),
                                                     TEST_CONTRIBUTION_CREATED_ON, TEST_CONTRIBUTION_UPDATED_ON, TEST_BRANCH_NAME);

        contribution.tags.add(TagTest.createTestInstance());
        contribution.contributionEvents.add(new ContributionEvent(ContributionEventType.CONTRIBUTION_GENERATED));

        return contribution;
    }
}
