package update;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.dispatch.ExecutionContexts;
import akka.event.EventStream;
import akka.pattern.Patterns;
import akka.testkit.TestKit;
import org.junit.Assert;
import org.junit.rules.ExternalResource;
import scala.Function1;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.runtime.AbstractFunction1;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class ActorRule extends ExternalResource {

    private static final String GIMME = "gimme!";

    private ActorSystem system;

    private TestKit testKit;

    @Override
    protected void before() throws Throwable {
        system = ActorSystem.create();
        testKit = new TestKit(system);
    }

    @Override
    protected void after() {
        system.shutdown();
    }

    public ActorRef actorOf(Class<? extends UntypedActor> actor) {
        return system.actorOf(new Props(actor));
    }

    public EventStream eventStream() {
        return system.eventStream();
    }

    public TestKit getTestKit() {
        return testKit;
    }

    private static class LetterBox extends UntypedActor {

        private LinkedList<Object> stash = new LinkedList<>();

        private ActorRef target = null;

        @Override
        public void onReceive(Object message) throws Exception {
            if (GIMME.equals(message)) {
                sendMessage();
            } else {
                receiveMessage(message);
            }
        }

        private void sendMessage() {
            if (stash.isEmpty()) {
                target = getSender();
            } else {
                notify(getSender());
            }
        }

        private void receiveMessage(Object message) {
            stash.add(message);
            if (target != null) {
                notify(target);
                target = null;
            }
        }

        private void notify(ActorRef target) {
            target.tell(stash, getSelf());
        }
    }

    @SuppressWarnings({"unchecked"})
    public <T> Future<T> expectMsg(Class<T> cls, long timeout) {
        ActorRef actor = actorOf(LetterBox.class);
        eventStream().subscribe(actor, cls);

        Future<Object> response = Patterns.ask(actor, GIMME, timeout);
        return response.map((Function1<Object, T>) lastReply, ExecutionContexts.global());
    }

    public void expectNoMsgWithinTimeout(Class<?> cls, long timeoutInMs) throws Exception {
        ActorRef actor = actorOf(LetterBox.class);
        eventStream().subscribe(actor, cls);

        Future<Object> response = Patterns.ask(actor, GIMME, timeoutInMs);
        try {
            Object value = Await.result(response, Duration.create(timeoutInMs, TimeUnit.MILLISECONDS));
            Assert.fail(value + " was returned");
        } catch (TimeoutException e) {
            // Deliberately blank since we expect no result within the timeout
        }

    }

    private static AbstractFunction1<?, ?> lastReply = new AbstractFunction1<Object, Object>() {
        @Override
        public Object apply(Object response) {
            LinkedList<?> replies = (LinkedList<?>) response;
            return replies.getLast();
        }
    };

}
